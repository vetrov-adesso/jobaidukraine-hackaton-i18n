package com.jobaidukraine.i18n;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(
        classes = I18nApplication.class)
@AutoConfigureMockMvc
@TestPropertySource(
        locations = "classpath:application-test.properties")
public class I18nTagsControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;
    @BeforeEach
    public void setup() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
    }

    @Test
    public void testI18nNoText() throws Exception {
        this.mockMvc.perform(get("/i18ntags").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("translations.[*].targetLanguage", containsInAnyOrder("de", "en", "uk", "ru")))
                .andExpect(jsonPath("translations.[*].translatedText", containsInAnyOrder("Welt", "world", "світ", "Мир")));
    }

    @Test
    public void testI18nWithText() throws Exception {
        this.mockMvc.perform(get("/i18ntags?text=хайдельберг").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("translations.[*].targetLanguage", containsInAnyOrder("de", "en", "uk", "ru")))
                .andExpect(jsonPath("translations.[*].translatedText", containsInAnyOrder("Heidelberg", "Гейдельберг", "Heidelberg", "Гейдельберг")));
    }
}

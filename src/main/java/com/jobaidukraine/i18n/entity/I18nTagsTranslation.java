package com.jobaidukraine.i18n.entity;

public class I18nTagsTranslation {
    private String translatedText;
    private String targetLanguage;

    public I18nTagsTranslation() {
    }

    public String getTranslatedText() {
        return translatedText;
    }

    public void setTranslatedText(final String translatedText) {
        this.translatedText = translatedText;
    }

    public String getTargetLanguage() {
        return targetLanguage;
    }

    public void setTargetLanguage(final String targetLanguage) {
        this.targetLanguage = targetLanguage;
    }
}

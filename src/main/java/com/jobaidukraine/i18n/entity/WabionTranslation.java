package com.jobaidukraine.i18n.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WabionTranslation {
    private String translatedText;
    private String detectedSourceLanguage;

    public WabionTranslation() {
    }

    public String getTranslatedText() {
        return translatedText;
    }

    public void setTranslatedText(final String translatedText) {
        this.translatedText = translatedText;
    }

    public String getDetectedSourceLanguage() {
        return detectedSourceLanguage;
    }

    public void setDetectedSourceLanguage(final String detectedSourceLanguage) {
        this.detectedSourceLanguage = detectedSourceLanguage;
    }
}

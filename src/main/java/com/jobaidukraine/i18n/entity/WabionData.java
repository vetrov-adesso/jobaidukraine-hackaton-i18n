package com.jobaidukraine.i18n.entity;

import java.util.List;

public class WabionData {
    private List<WabionTranslation> translations;

    public WabionData() {
    }

    public List<WabionTranslation> getTranslations() {
        return translations;
    }

    public void setTranslations(final List<WabionTranslation> translations) {
        this.translations = translations;
    }
}

package com.jobaidukraine.i18n.entity;

import java.util.List;

public class I18nTagsResponse {
    List<I18nTagsTranslation> translations;

    public I18nTagsResponse() {
    }

    public List<I18nTagsTranslation> getTranslations() {
        return translations;
    }

    public void setTranslations(final List<I18nTagsTranslation> translations) {
        this.translations = translations;
    }
}

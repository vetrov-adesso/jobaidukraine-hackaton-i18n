package com.jobaidukraine.i18n.entity;

public class WabionResponse {
    private WabionData data;

    public WabionResponse() {
    }

    public WabionData getData() {
        return data;
    }

    public void setData(final WabionData data) {
        this.data = data;
    }
}

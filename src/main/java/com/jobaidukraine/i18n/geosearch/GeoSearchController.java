package com.jobaidukraine.i18n.geosearch;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

@RestController
public class GeoSearchController {
    private final RestTemplate restTemplate;

    @Value("${geo.search.url}")
    private String geoSearchUrl;

    @Autowired
    public GeoSearchController(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @GetMapping("/geo")
    public String findLocation(@RequestParam(name = "searchLocation") String searchLocation) {
        String url = geoSearchUrl + "?q=" + searchLocation;
        return this.restTemplate.getForObject(url, String.class);
    }
}

package com.jobaidukraine.i18n;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.jobaidukraine.i18n.entity.I18nTagsResponse;
import com.jobaidukraine.i18n.entity.I18nTagsTranslation;
import com.jobaidukraine.i18n.entity.WabionResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class I18nTagsController {

    private RestTemplate restTemplate;

    @Value("${target-languages}")
    private String targetLanguages;

    @Value("${wabion.translate-url}")
    private String translateUrl;

    @Value("${wabion.key}")
    private String key;

    @Autowired
    public I18nTagsController(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @GetMapping("/i18ntags")
    public I18nTagsResponse greeting(@RequestParam(value = "text", defaultValue = "world") String text) {

        I18nTagsResponse response = new I18nTagsResponse();
        List<I18nTagsTranslation> translations = new ArrayList<>();
        response.setTranslations(translations);

        Arrays.stream(targetLanguages.split(",\\s*")).forEach(targetLanguage -> {
            translations.add(buildTranslation(text, targetLanguage));
        });

        return response;
    }

    private I18nTagsTranslation buildTranslation(String text, String targetLanguage) {
        WabionResponse wResponse = restTemplate.getForObject(translateUrl + "?key={key}&q={q}&target={target}",
                                                             WabionResponse.class,
                                                             Map.of("key", key, "q", text, "target", targetLanguage));

        I18nTagsTranslation translation = new I18nTagsTranslation();
        translation.setTranslatedText(wResponse.getData().getTranslations().get(0).getTranslatedText());
        translation.setTargetLanguage(targetLanguage);

        return translation;
    }
}
